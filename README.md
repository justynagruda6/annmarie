# AnnMarie
This is my first project, and it's based on a template from Weekly WebDev Challenge. A simple landing page for a photographer created using HTML, SCSS and jQuery 3.5.0.<br>
Live:  [_https://ann-marie-just-gru.netlify.app_](https://ann-marie-just-gru.netlify.app).
