function removeActive() {
    $('.link span').removeClass('active')
}

$('.link').on('click', function () {
    removeActive()
    $($(this).find('.anchor')[0]).addClass('active')
})


$('.name').on('click', function () {
    removeActive()
})

function toggleMobileNav() {
    $('.links-container').toggleClass('hide-links-container')
}

$('.fa-bars, .link').on('click', function () {
    toggleMobileNav()
})
